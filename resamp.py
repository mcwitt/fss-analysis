import numpy as np
import pandas as pd
from numpy.random import random_integers

def jackknife_average(a):
    return (a.sum() - a)/(len(a) - 1.)

def jackknife(func):
    def _eval(df):
        resamp = df.apply(jackknife_average)
        
        if isinstance(func, str):
            result = resamp.eval(func)
        else:
            result = func(resamp)

        return pd.Series({'mean': result.mean(),
                          'serr': np.sqrt(len(result)-1.)*result.std()})
    return _eval

def bootstrap(func, nboot=1000):
    def _eval(df):
        n = len(df)
        result = []

        for i in xrange(nboot):
            isamp = random_integers(0, n-1, n)
            samp = df.iloc[isamp]
            
            if isinstance(func, str):
                result.append(samp.eval(func))
            else:
                result.append(func(samp))
                
        result = pd.DataFrame.from_records(result)
                
        return pd.DataFrame({'mean': result.mean(),
                             'serr': result.std()})
    return _eval

