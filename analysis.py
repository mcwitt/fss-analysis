import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from itertools import cycle
from scipy.interpolate import UnivariateSpline

Tcd = {
        5: 8.77846,
        6: 10.8346 
      }

def load_data(files, params=None):
    data = pd.concat(pd.read_table(f, delim_whitespace=True) for f in files)
    #del data[data.columns[0]] # work around bug in pandas C parser
    del data['ver']
    data.set_index(['bc', 'd','L','T','oct','seed','k'], inplace=True)
    data.sortlevel(inplace=True)

    if params:
        for key, value in params.iteritems():
            data = data.xs(value, level=key)

    return data

def scalefunc(T, L, Tc, yT=0., A=0., lm=0.):
    return L**yT*(T - Tc + A/L**lm)

def scalefuncinv(x, L, Tc, yT=0., A=0., lm=0.):
    return Tc - A/L**lm + x/L**yT

def plot_binder(data,
                d=5,
                bc='periodic',
                Tc=None,
                scaling=None,
                ax=None,
                xlabel=None,
                ylabel=None,
                splinepoints=100,
                colors=list('bgrcmy'),
                legend=True,
                **kwargs):

    colors = cycle(colors)
    
    if ax is None:
        ax = plt.gca()
        
    if Tc is None:
        Tc = Tcd[d]
        
    binder = data.xs(d, level='d')
    
    for L, df in binder.groupby(level='L'):
        T = df.index.get_level_values('T').values
        
        if scaling is None:
            x = scalefunc(T, L, Tc)
        else:
            x = scalefunc(T, L, Tc, **scaling)
            
        y = df['mean']
        yerr = df['serr']
        color = colors.next()

        ax.errorbar(x, y, yerr, label='${}$'.format(L), 
                    ls='', color=color, **kwargs)
            
        if splinepoints and splinepoints > 1:
            func = UnivariateSpline(x, y, 1./yerr)
            xs = np.linspace(x[0], x[-1], splinepoints)
            ys = func(xs)
            ax.plot(xs, ys, color=color)
            
        if xlabel is None:
            if scaling is None:
                xlabel = '$T-T_c$'
            else:
                xlabel = '$L^{y_T}(T - T_c + A/L^{\lambda})$'
            
        if ylabel is None:
            ylabel = '$g$'
            
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        
        if legend: ax.legend(title='$d={}$, {}'.format(d, bc), loc='best')
